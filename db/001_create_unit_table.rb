# frozen_string_literal: true

Sequel.migration do
  up do
    create_table(:units) do
      String :id, primary_key: true
      column :position, :geography, null: false
      Float :altitude, default: 0
      String :type
      String :name, null: true
      String :pilot, null: true
      String :group, null: true
      Integer :coalition
      Integer :heading
      Integer :speed
      Time :updated_at
      Boolean :deleted
      # TODO: GIST Index on the position
    end
  end

  down do
    drop_table(:units)
  end
end
